apiVersion: unfurl/v1alpha1
kind: Ensemble
spec:
  shared:
    compute:
      dockerhost:
        type: unfurl.nodes.HttpsProxyContainerComputeHost
      db-dockerhost:
        type: unfurl.nodes.DockerComputeHost
  deployment_blueprints:
    +include:
      file: deployment-blueprints.yaml
      repository:
        name: types
        url: https://unfurl.cloud/onecommons/unfurl-types.git
  service_template:
    description: Etherpad is a real-time collaborative editor for the web
    metadata:
      template_name: Etherpad
      template_version: '0.1'
      image: '# url to image'
      primaryDeploymentBlueprint: gcp
      instructions:
        start: Etherpad is a real-time collaborative editor for the web
        end: Etherpad is a real-time collaborative editor for the web is deployed
          and available as srv-captain--$$cap_appname. Note that the application may
          take some time to become available.
      displayName: Etherpad
      isOfficial: true
      description: Etherpad is a real-time collaborative editor for the web
      documentation: 'https://github.com/ether/etherpad-lite/blob/develop/doc/docker.md '
    repositories:
      types:
        url: https://unfurl.cloud/onecommons/unfurl-types.git
    imports:
    - file: service-template.yaml
      repository: types
    node_types:
      EtherpadApp:
        derived_from: unfurl.nodes.WebApp
        properties:
          etherpad_version:
            type: string
            title: Etherpad Docker Image
            description: Check out their Docker page for the valid tags https://hub.docker.com/r/etherpad/etherpad/tags
              - default is 1.8.0 as of 2020-04-09
            default: 1.8.0
            metadata:
              user_settable: true
          mysql_version:
            type: string
            title: MySQL Version
            description: Check out their Docker page for the valid tags https://hub.docker.com/r/library/mysql/tags/
            default: '5.7'
            constraints:
            - pattern: ^([^\s^\/])+$
            metadata:
              user_settable: true
          subdomain:
            type: string
            title: Subdomain
            description: Choose a subdomain for your deployment. A subdomain of 'www',
              will be at www.your.domain
            default: etherpad
            metadata:
              user_settable: true
          database_name:
            type: string
            title: Database Name
            default: etherpad
            description: Preferred name for Etherpad database
            metadata:
              user_settable: true
        requirements:
        - container:
            relationship: unfurl.relationships.Configures
            node: container
        - db:
            node: MySQLDB
            relationship: unfurl.relationships.Configures
            description: External database for Etherpad
            occurrences:
            - 1
            - 1
            metadata:
              title: Database Instance
            node_filter:
              properties:
              - database_name:
                  eval: .configured_by::database_name
    topology_template:
      substitution_mappings:
        node: the_app
      outputs:
        url:
          value:
            eval: ::the_app::url
      node_templates:
        container:
          type: unfurl.nodes.ContainerService
          properties:
            container:
              image: etherpad/etherpad:{{'.configured_by::etherpad_version' | eval}}
              ports:
              - 9001:9001
              environment:
                eval:
                  to_env:
                    NODE_ENV: production
                    DB_TYPE:
                      eval:
                        if: .configured_by::.targets::db
                        then: .configured_by::.targets::db::database_type
                        else: null
                    DB_HOST:
                      eval:
                        if: .configured_by::.targets::db
                        then: .configured_by::.targets::db::host
                        else: null
                    DB_PORT: '3306'
                    DB_NAME:
                      eval:
                        if: .configured_by::.targets::db
                        then: .configured_by::.targets::db::database_name
                        else: null
                    DB_USER:
                      eval:
                        if: .configured_by::.targets::db
                        then: .configured_by::.targets::db::root_user
                        else: null
                    DB_PASS:
                      eval:
                        if: .configured_by::.targets::db
                        then: .configured_by::.targets::db::root_password
                        else: null
                    DB_CHARSET: utf8mb4
                    TRUST_PROXY: 'true'
              volumes: []
          requirements:
          - host:
              node: dockerhost
        the_app:
          type: EtherpadApp
